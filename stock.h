#ifndef _STOCK_H_
#define _STOCK_H_

/*
  stock.h
  Author: M00663746
  Created: 26/03/2021
  Updated: 13/05/2021
*/

#include <iostream>
#include <ostream>
#include <string>
#include <ctime>


//Sales Class
class Sale {
private:
  std::string type;
  std::string title;
  int quantity;
  char* time;
  double boughtPrice;	
public:
  Sale();
  Sale(std::string type, std::string title, int quantity, char* time,
       double boughtPrice);

  //Setter methods
  void setType(std::string type);
  void setTitle(std::string title);
  void setQuantity(int quantity);
  void setTime(char* time);
  void setBoughtPrice(double boughtPrice);

  //Getter methods
  std::string getType();
  std::string getTitle();
  int getQuantity();
  char* getTime();
  double getBoughtPrice();
};


//Product Class
class Product {
private:
  std::string title;
  int quantity;
  double price;
public:
  Product();
  Product(std::string title, int quantity, double price);
  void add();
  void sell();
  void restock();

  //Setter methods
  void setTitle(std::string title);
  void setQuantity(int quantity);
  void setPrice(double price);

  //Getter methods
  std::string getTitle();
  int getQuantity();
  double getPrice();  
};


// CD Class derived from Product
class CD : public Product {
private:
  std::string artist;
  int year;
public:
  CD();
  CD(std::string artist, int year);
  friend std::ostream& operator<<(std::ostream& out, const CD& cd);

  //Setter methods
  void setArtist(std::string artist);
  void setYear(int year);

  //Getter methods
  std::string getArtist();
  int getYear();
};


//DVD Class derived from Product
class DVD : public Product {
private:
  std::string director;
  int year;
  int length;
public:
  DVD();
  DVD(std::string director, int year, int length);
  friend std::ostream& operator<<(std::ostream& out, const DVD& dvd);

  //Setter methods
  void setDirector(std::string director);
  void setYear(int year);
  void setLength(int length);

  //Getter methods
  std::string getDirector();
  int getYear();
  int getLength();
};


//Magazine Class derived from Product
class Magazine : public Product {
private:
  std::string editor;
  int year;
public:
  Magazine();
  Magazine(std::string editor, int year);
  friend std::ostream& operator<<
  (std::ostream& out, const Magazine& magazine);

  //Setter methods
  void setEditor(std::string editor);
  void setYear(int year);

  //Getter methods
  std::string getEditor();
  int getYear();
};


//Book Class derived from Product
class Book : public Product {
private:
  std::string author;
  int pages;
public:
  Book();
  Book(std::string author, int pages);
  friend std::ostream& operator<<(std::ostream& out, const Book& book);

  //Setter methods
  void setAuthor(std::string author);
  void setPages(int pages);

  //Getter methods
  std::string getAuthor();
  int getPages();
};


#endif
