CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY : all
all : mainStock

mainStock : mainStock.cpp stock.o
	$(CXX) $(CXXFLAGS) -o $@ $^

stock.o : stock.cpp stock.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean :
	$(RM) *.o
	$(RM) mainStock

