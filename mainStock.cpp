/*2
  mainStock.cpp
  Author: M00663746
  Created: 26/03/2021
  Updated: 13/05/2021
*/

#include <iostream>
#include <vector>
#include "stock.h"

/*Product Choice Function*/
void displayChoices()
{
  std::cout << "1. CD" << std::endl;
  std::cout << "2. DVD" << std::endl;
  std::cout << "3. Magazine" << std::endl;
  std::cout << "4. Book" << std::endl;
}

/*Product Struct*/
struct sProduct {
   std::string title;
   int quantity;
   double price;
   int year;
   std::string artist;
   std::string director;
   int length;
   std::string editor;
   std::string author;
   int pages;
};

/* Product Struct Function*/
sProduct getUserData(int type)
{
  sProduct s_pr;
  std::cout << "\n Enter Title: ";
  std::cin >> s_pr.title;
  std::cout << "\n Enter Quantity: ";
  std::cin >> s_pr.quantity;
  std::cout << "\n Enter Price: ";
  std::cin >> s_pr.price;

  switch(type)
    {
    case 1: //CD
      std::cout << "\n Enter Artist: ";
      std::cin >> s_pr.artist;
      std::cout << "\n Enter Year: ";
      std::cin >> s_pr.year;
      break;
    case 2: //DVD
      std::cout << "\n Enter Director: ";
      std::cin >> s_pr.director;
      std::cout << "\n Enter Year: ";
      std::cin >> s_pr.year;
      std::cout << "\n Enter Length: ";
      std::cin >> s_pr.length;
      break;
    case 3: //Magazine
      std::cout << "\n Enter Editor: ";
      std::cin >> s_pr.editor;
      std::cout << "\n Enter Year: ";
      std::cin >> s_pr.year;
      break;
    case 4: //Book
      std::cout << "\n Enter Author: ";
      std::cin >> s_pr.author;
      std::cout << "\n Enter Pages: ";
      std::cin >> s_pr.pages;
      break;
    default:
      std::cout << "No match" << std::endl;
    }
  return s_pr;
}


/*
  Generate time
  @param time
  @return date_time
*/

 char* generateTime()
 {
   //current date & time
   time_t now = time(0);
   char* date_time = ctime(&now);
   return date_time;
 }

//Main
int main()
{
  std::vector<CD> myCDs;
  std::vector<DVD> myDVDs;
  std::vector<Magazine> myMagazines;
  std::vector<Book> myBooks;
  std::vector<Sale> mySales;
  sProduct str_prod;
  CD cd;
  DVD dvd;
  Magazine mag;
  Book book;
  Sale sale;
  int userChoice;
  int userOption;
  std::string response;
  std::string title;
  int quantity;
  double price;
  char* date_t;
  unsigned long i;
  bool find;

  //Main Menu
  do
    {
      std::cout << "-----Welcome to the Stock Management System-----" <<
	std::endl << std::endl;
      std::cout << "Please enter your choice" << std::endl;
      std::cout << "1. Add items" << std::endl;
      std::cout << "2. Sell items" << std::endl;
      std::cout << "3. Restock items" << std::endl;
      std::cout << "4. Update items" << std::endl << std::endl;

      std::cin >> userChoice;

      switch(userChoice)
	{
	case 1: //Adding Products
	  std::cout << "Which product would you like to add?" << std::endl;
	  displayChoices();
	  
	  std::cin >> userOption;

	  switch(userOption)
	    {
	    case 1: //CD
	      str_prod = getUserData(1);
	      cd.setTitle(str_prod.title);
	      cd.setQuantity(str_prod.quantity);
	      cd.setPrice(str_prod.price);
	      cd.setArtist(str_prod.artist);
	      cd.setYear(str_prod.year);
	      myCDs.push_back(cd);
	      std::cout << "\n" << cd.getTitle() <<
		" has been added successfully" << std::endl;
	      break;
	    case 2: //DVD
	      str_prod = getUserData(2);
	      dvd.setTitle(str_prod.title);
	      dvd.setQuantity(str_prod.quantity);
	      dvd.setPrice(str_prod.price);
	      dvd.setDirector(str_prod.director);
	      dvd.setYear(str_prod.year);
	      dvd.setLength(str_prod.length);
	      myDVDs.push_back(dvd);
	      std::cout << "\n" << dvd.getTitle() <<
		" has been added successfully" << std::endl;
	      break;
	    case 3: //Magazine
	      str_prod = getUserData(3);
	      mag.setTitle(str_prod.title);
	      mag.setQuantity(str_prod.quantity);
	      mag.setPrice(str_prod.price);
	      mag.setEditor(str_prod.editor);
	      mag.setYear(str_prod.year);
	      myMagazines.push_back(mag);
	      std::cout << "\n" << mag.getTitle() <<
		" has been added successfully" << std::endl;
	      break;
	    case 4: //Book
	      str_prod = getUserData(4);
	      book.setTitle(str_prod.title);
	      book.setQuantity(str_prod.quantity);
	      book.setPrice(str_prod.price);
	      book.setAuthor(str_prod.author);
	      book.setPages(str_prod.pages);
	      myBooks.push_back(book);
	      std::cout << "\n" << book.getTitle() <<
		" has been added successfully" << std::endl;
	      break;
	    default:
	      std::cout << "Enter a valid choice" << std::endl;
	    }
	  break;
	case 2: //Selling Products
	  std::cout << "Which product would you like to sell?"
		    << std::endl;
	  displayChoices();

	  std::cin >> userOption;
	  std::cout << "\n Enter Title: ";
	  std::cin >> title;
	  std::cout << "\n Enter Quantity: ";
	  std::cin >> quantity;
	  std::cout << "\n Enter Price: ";
	  std::cin >> price;
	  date_t = generateTime();

	  switch(userOption)
	    {
	    case 1: //CD
	      sale.setType("CD");
	      sale.setTitle(title);
	      sale.setQuantity(quantity);
	      sale.setTime(date_t);
	      sale.setBoughtPrice(price);
	      mySales.push_back(sale);
	      std::cout << "\n Sale of " << sale.getTitle() <<
		" has been performed successfully" << std::endl;

	      //Update quantity
	      i = 0;
	      find = false;

	      while (i<myCDs.size() && find == false)
		{
		  if (myCDs[i].getTitle() == title)
		    {
		      find = true;
		    }
		  i++;
		}
	      i--;
	      if (find == true)
		{
		  int currentQuantity = myCDs[i].getQuantity();
		  myCDs[i].setQuantity(currentQuantity - quantity);
		}
	      std::cout << "Quantity of " << sale.getTitle() <<
		" has been updated successfully" << std::endl;
	      break;
	    case 2: //DVD
	      sale.setType("DVD");
	      sale.setTitle(title);
	      sale.setQuantity(quantity);
	      sale.setTime(date_t);
	      sale.setBoughtPrice(price);
	      mySales.push_back(sale);
	      std::cout << "\n Sale of " << sale.getTitle() <<
		" has been performed successfully" << std::endl;

	      //Update quantity
	      i = 0;
	      find = false;

	      while (i<myDVDs.size() && find == false)
		{
		  if (myDVDs[i].getTitle() == title)
		    {
		      find = true;
		    }
		  i++;
		}
	      i--;
	      if (find == true)
		{
		  int currentQuantity = myDVDs[i].getQuantity();
		  myDVDs[i].setQuantity(currentQuantity - quantity);
		}
	      std::cout << "Quantity of " << sale.getTitle() <<
		" has been updated successfully" << std::endl;
	      break;
	    case 3: //Magazine
	      sale.setType("Magazine");
	      sale.setTitle(title);
	      sale.setQuantity(quantity);
	      sale.setTime(date_t);
	      sale.setBoughtPrice(price);
	      mySales.push_back(sale);
	      std::cout << "\n Sale of " << sale.getTitle() <<
		" has been performed successfully" << std::endl;

	      //Update quantity
	      i = 0;
	      find = false;

	      while (i<myMagazines.size() && find == false)
		{
		  if (myMagazines[i].getTitle() == title)
		    {
		      find = true;
		    }
		  i++;
		}
	      i--;
	      if (find == true)
		{
		  int currentQuantity = myMagazines[i].getQuantity();
		  myMagazines[i].setQuantity(currentQuantity - quantity);
		}
	      std::cout << "Quantity of " << sale.getTitle() <<
		" has been updated successfully" << std::endl;
	      break;
	    case 4: //Book
	      sale.setType("Book");
	      sale.setTitle(title);
	      sale.setQuantity(quantity);
	      sale.setTime(date_t);
	      sale.setBoughtPrice(price);
	      mySales.push_back(sale);
	      std::cout << "\n Sale of " << sale.getTitle() <<
		" has been performed successfully" << std::endl;

	      //Update quantity
	      i = 0;
	      find = false;

	      while (i<myBooks.size() && find == false)
		{
		  if (myBooks[i].getTitle() == title)
		    {
		      find = true;
		    }
		  i++;
		}
	      i--;
	      if (find == true)
		{
		  int currentQuantity = myBooks[i].getQuantity();
		  myBooks[i].setQuantity(currentQuantity - quantity);
		}
	      std::cout << "Quantity of " << sale.getTitle() <<
		" has been updated successfully" << std::endl;
	      break;
	    }
	  break;
	case 3: //Restocking
	  break;
	case 4: //Updating
	  break;
	}
      std::cout << "\n Do you want to exit the system? (Y/N)";
      std::cin >> response;
    } while (userChoice <=0 || userChoice > 5 || response == "N");
  return 0;
}




 
   
 
  
