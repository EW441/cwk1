
/*
  stock.cpp
  Author: M00663746
  Created: 26/03/2021
  Updated: 13/05/2021
*/

#include "stock.h"


/*Sales Class*/

//Constructors
Sale::Sale()
{
  type = "";
  title = "";
  quantity = 0;
  time = (char*)"";
  boughtPrice = 0.0;
}

Sale::Sale(std::string type, std::string title, int quantity, char* time,
	   double boughtPrice)
{
  this->type = type;
  this->title = title;
  this->quantity = quantity;
  this->time = time;
  this->boughtPrice = boughtPrice;
}

//Setter methods
void Sale::setType(std::string type)
{
  type = type;
}
void Sale::setTitle(std::string title)
{
  title = title;
}
void Sale::setQuantity(int quantity)
{
  quantity = quantity;
}
void Sale::setTime(char* time)
{
  time = time;
}
void Sale::setBoughtPrice(double boughtPrice)
{
  boughtPrice = boughtPrice;
}

//Getter methods
std::string Sale::getType()
{
  return type;
}
std::string Sale::getTitle()
{
  return title;
}
int Sale::getQuantity()
{
  return quantity;
}
char* Sale::getTime()
{
  return time;
}
double Sale::getBoughtPrice()
{
  return boughtPrice;
}


/*Products Class*/

//Constructors
Product::Product()
{
  title = "";
  quantity = 0;
  price = 0.0;
}  

Product::Product(std::string title, int quantity, double price)
{
  this->title = title;
  this->quantity = quantity;
  this->price = price;
}

//Setter methods
void Product::setTitle(std::string title)
{
  title = title;
}
void Product::setQuantity(int quantity)
{
  quantity = quantity;
}
void Product::setPrice(double price)
{
  price = price;
}

//Getter methods
std::string Product::getTitle()
{
  return title;
}
int Product::getQuantity()
{
  return quantity;
}
double Product::getPrice()
{
  return price;
}


/*CD Class*/

//Constructors
CD::CD():Product::Product()
{
  artist = "";
  year = 0;
}


//Setter methods
void CD::setArtist(std::string artist)
{
  artist = artist;
}
void CD::setYear(int year)
{
  year = year;
}

//Getter methods
std::string CD::getArtist()
{
  return artist;
}
int CD::getYear()
{
  return year;
}


/*DVD Class*/

//Constructor
DVD::DVD():Product::Product()
{
  director = "";
  year = 0;
  length = 0;
}

//Setter methods
void DVD::setDirector(std::string director)
{
  director = director;
}
void DVD::setYear(int year)
{
  year = year;
}
void DVD::setLength(int length)
{
  length = length;
}

//Getter methods
std::string DVD::getDirector()
{
  return director;
}
int DVD::getYear()
{
  return year;
}
int DVD::getLength()
{
  return length;
}


/*Magazine Class*/

//Constructors
Magazine::Magazine():Product::Product()
{
  editor = "";
  year = 0;
}

//Setter methods
void Magazine::setEditor(std::string editor)
{
  editor = editor;
}
void Magazine::setYear(int year)
{
  year = year;
}

//Getter methods
std::string Magazine::getEditor()
{
  return editor;
}
int Magazine::getYear()
{
  return year;
}


/*Book Class*/

//Constructors
Book::Book():Product::Product()
{
  author = "";
  pages = 0;
}

//Setter methods
void Book::setAuthor(std::string author)
{
  author = author;
}
void Book::setPages(int pages)
{
  pages = pages;
}

//Getter methods
std::string Book::getAuthor()
{
  return author;
}
int Book::getPages()
{
  return pages;
}












